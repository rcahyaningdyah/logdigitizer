var help_icon = 'url(img/i_question12.png)';
var help_imgage_th_u_k = "img/th_u_k.png";

var help_Reference = '<table class="table table-hover table-condensed"><tbody>';
	help_Reference += '<thead><tr><th>Name</th><th>Description</th></tr></thead><tbody>';
	help_Reference += ('<tr><td><b>CGR</b></td><td>Computed gamma ray log</td></tr>'); 
	help_Reference += ('<tr><td><b>SGR</b></td><td>Standard gamma ray log</td></tr>');
	help_Reference += ('<tr><td><b>GR</b></td><td>Gamma ray log</td></tr>');
	help_Reference += ('<tr><td><b>SP</b></td><td>Total Gamma Ray</td></tr>');
	help_Reference += ('<tr><td><b>CAL</b></td><td>Caliper log</td></tr>');
	help_Reference += '</tbody></table>';

var help_DualInduction = '<table class="table table-hover table-condensed"><tbody>';
	help_DualInduction += '<thead><tr><th>Name</th><th>Description</th></tr></thead><tbody>';
	help_DualInduction += ('<tr><td><b>SFLU</b></td><td>Spherically focused resistivity log</td></tr>'); 
	help_DualInduction += ('<tr><td><b>ILM</b></td><td>Medium Induction Log</td></tr>');
	help_DualInduction += ('<tr><td><b>ILD</b></td><td>Depp Induction Log</td></tr>');
	help_DualInduction += ('<tr><td><b>AT10</b></td><td>Array Induction Resistivity A10</td></tr>');
	help_DualInduction += ('<tr><td><b>AT30</b></td><td>Array Induction Resistivity A30</td></tr>');
	help_DualInduction += ('<tr><td><b>AT60</b></td><td>Array Induction Resistivity A60</td></tr>');
	help_DualInduction += ('<tr><td><b>AT90</b></td><td>Array Induction Resistivity A90</td></tr>');
	help_DualInduction += '</tbody></table>';

var help_LithoDensity = '<table class="table table-hover table-condensed"><tbody>';
	help_LithoDensity += '<thead><tr><th>Name</th><th>Description</th></tr></thead><tbody>';
	help_LithoDensity += ('<tr><td><b>DPHI</b></td><td>Density porosity Log</td></tr>'); 
	help_LithoDensity += ('<tr><td><b>NPHI</b></td><td>Neutron porosity Log</td></tr>');
	help_LithoDensity += ('<tr><td><b>RHOB</b></td><td>Bulk density log</td></tr>');
	help_LithoDensity += ('<tr><td><b>PE</b></td><td>Photo-electric index log</td></tr>');
	help_LithoDensity += '</tbody></table>';

var help_SpectralGammaRay = '<table class="table table-hover table-condensed"><tbody>';
	help_SpectralGammaRay += '<thead><tr><th>Name</th><th>Description</th></tr></thead><tbody>';
	help_SpectralGammaRay += ('<tr><td><b>POTA</b></td><td>Potassium log</td></tr>'); 
	help_SpectralGammaRay += ('<tr><td><b>Uran</b></td><td>Uranium log</td></tr>');
	help_SpectralGammaRay += ('<tr><td><b>Thor</b></td><td>Thorium log</td></tr>');
	help_SpectralGammaRay += '</tbody></table>';

var help_SpectralGR_Ratio = '<table class="table table-hover table-condensed"><tbody>';
	help_SpectralGR_Ratio += '<thead><tr><th>Name</th><th>Description</th></tr></thead><tbody>';
	help_SpectralGR_Ratio += ('<tr><td><b>Th/K</b></td><td>Thorium/Potassium log</td></tr>'); 
	help_SpectralGR_Ratio += ('<tr><td><b>Th/U</b></td><td>Thorium/Uranium log</td></tr>');
	help_SpectralGR_Ratio += '</tbody></table>';

var help_Custom1 = '<table class="table table-hover table-condensed"><tbody>';
	help_Custom1 += '<thead><tr><th>Name</th><th>Description</th></tr></thead><tbody>';
	help_Custom1 += ('<tr><td colspan="2">Not Available</td></tr>');
	help_Custom1 += '</tbody></table>';

var help_Color_lith = '<img src="' + help_imgage_th_u_k + '" />';

var help_Stratigraphic_Units = 'Name indicates top of formation';
