$(function() {
	var controllingChart;

	var defaultTickInterval = 10;
	var currentTickInterval = defaultTickInterval;
	var base_apps_url = 'http://'+window.location.hostname+'/logdigitizer';

	$(document).ready(function() {
		//initialInterface();
		$('[data-toggle="popover"]').popover({
    		trigger:'hover focus',
    		html : true
    	});
    	
		$('#btnResetZoom').css("display","none");
		$('#btnResetZoom').click(function(){
			$('#btnResetZoom').css("display","none");
			
			$('#busyModal').modal('show');
			setTimeout(function (){
				synZoom(null, null, 'none');
			}, 1000);
		});

	    $("#aboutModal").draggable({
	    	handle: ".modal-header"
		});
	    $("#helpDialog").draggable({
	    	handle: ".modal-header"
		});
    		
		$('#iconOpenLogList').click(function(){
			Print_Unploted_Log_List();				// in las_list.js
			
			//$('#modalLogList').modal('show');
		});
		
		$(icProgress).css("visibility", "hidden");
		
		// disable two buttons when load window is on
		$("#iconOpen").prop("disabled",true);
		$("#iconOpenLogList").prop("disabled",true);

		$(iconOpen).click(function(){
			
			//$("#btnSearchLasfile").css("visibility", "visible");			// show search las file button once the plots are loading
			//$("#canelFile").css("visibility", "visible");				// show cancel button once the plots are loading

			$("#fileInput").prop("disabled",false);						// enable fileinput button	
			$("#canelFile").prop("disabled",false);						// enable cancelfile button

			//$(loadingWindow).css("display","block");
			$(loadingWindow).css("display","block");
			$("#iconOpen").prop("disabled",true);
			$("#iconOpenLogList").prop("disabled",true);
		});

		$("#canelFile").click(function(){
			$(loadingWindow).jqxWindow('close');
		});
		
		$(loadingWindow).on("close", function(){
			$("#iconOpen").prop("disabled",false);
			if (FIRSTTIME) 
				$("#iconOpenLogList").prop("disabled",true);
			else
				$("#iconOpenLogList").prop("disabled",false);
		});
		
		createPopWindow();					// This function is in untils.js
		
		window.onresize = function (){
			initialInterface();
		}
		
		window.onload = function() {
			/*
			 * This segment retrieve client ip
			 */
			
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = "http://www.telize.com/jsonip?callback=DisplayIP";
            document.getElementsByTagName("head")[0].appendChild(script);
            
            // end
            /*
            $.ajax({
            	type: "GET",
        		url: "http://www.telize.com/jsonip",
        		contentType: "application/json; charset=utf-8",
        		dataType: "json",
        		success: DisplayIP,
        		failure: alert("askdfjsakd")
            })
			*/
			var sUrlLas = location.search.substr(location.search.indexOf("?")+1).split("=");
			if ( sUrlLas.length == 2 ){
				var a = sUrlLas[1];
				
				BEGINNING_TIME = (new Date()).getTime();
				
				DATASOURCE_TYPE = "online";
				NO_CURVE_TO_PLOT = true;
				FILENAME_LAS = a.substring(a.lastIndexOf("index.html")+1, a.length); 
				
				var f = sUrlLas[1];
				var ft = f.substring(f.lastIndexOf(".") + 1, f.length);
				if (ft.toUpperCase() == "LAS") {
					$("#btnResetZoom").css("visibility", "hiden");
					console.log("Reading remote file." + currentTime());
					$('#uploadFile').removeClass('file_error');
					//$('#loadFile').css("visibility", "visible");
					$(icProgress).css("visibility", "visible");
					
					var xhr = createCORSRequest('GET', sUrlLas[1]);
					
					if ( xhr ) {
						xhr.send(); 
						xhr.onreadystatechange = function(){
							if (xhr.readyState==4 && xhr.status==200){
								console.log(currentTime() + ". ->File returned from remote.");
								$(icProgress).css("visibility", "visible");
								document.getElementById("uploadFile").value = FILENAME_LAS;
								$('#loadFile').css("visibility", "hidden");
								$('#fileInput').css("visibility", "hidden");
								
								setTimeout(function (){
									processLasfile(xhr.responseText);
									chartingLasdata();
								}, 2000);
							}
							if (xhr.readyState==1 && xhr.status==200){
								//$(icProgress).css("visibility", "visible");
								document.getElementById("uploadFile").value = FILENAME_LAS;
								$('#loadFile').css("visibility", "visible");
							}
						}
					}				
				}
				else if (ft.toUpperCase() == "XLSX"){
					
				}
				else
				{
					$('#uploadFile').addClass('file_error');
					
					$('#uploadFile').val("File not supported!");
					$('#loadFile').css("visibility", "hidden");
				}
				
			}

			/*
			 * validae the uploaded file
			 */
			$("#fileInput").on("change", function(){
				TurnOffButtons();									// the function is at the bottom of this file.
					
				$('#radioButtonLas').prop("checked", false);		// 
				$('#radioButtonExcel').prop("checked", false);		//
				
				if (fileInput.files.length>0){
					/*document.getElementById("uploadFile").value = FILENAME_LAS;
					var file = fileInput.files[0];*/
					document.getElementById("uploadFile").value = fileInput.files[0].name;
					var file = fileInput.files[0];
					
					if ( file!=undefined ) {
						var filename = file.name;
						FILE_TYPE = ( filename.substring(filename.lastIndexOf(".") + 1, filename.length) ).toUpperCase();
						
						if (FILE_TYPE == "LAS") {
							FILENAME_LAS = fileInput.files[0].name;
				
							$('#uploadFile').removeClass('file_error');
							$('#loadFile').css("visibility", "visible");
							$('#loadFile').css("display", "inline");
							
							$("#plotOptions").css("display","none");
							$("#excelSheets").css("display", "none");
						}
						else if (FILE_TYPE == "XLSX" || FILE_TYPE == "XLS"){
							//$("#excelSheets").css("display", "inline");
							
							$("#divExcelProcessing").css("display", "block");
							FILENAME_EXCEL = fileInput.files[0].name;
							
							angular.element(this).scope().fileChanged(this.files);			// AngularJS call in las_excel.js
							
							$("#divExcelProcessing").css("display", "none");
							if (logLine_Name.length>0) {
								$("#plotOptions").css("display","block");
								$('#plotOptions').css("visibility", "visible");
								$("#plotNew").css("display","inline");
							}
							else
							{
								$('#loadFile').css("visibility", "visible");
								$('#loadFile').css("display", "inline");
								
								$("#plotOptions").css("display","none");
								$("#excelSheets").css("display", "none");
							}
						}
						else
						{
							$('#uploadFile').addClass('file_error');
							
							$('#uploadFile').val("File not supported!");
							$('#loadFile').css("visibility", "hidden");
							
							$("#plotOptions").css("display","none");
						}
					}
				}
				else {
					$('#loadFile').css("visibility", "hidden");
					$('#plotOptions').css("visibility", "hidden");
					$('#uploadFile').val("Select a file");
				}
			});
			
			// if the plot options change
			$("input[name=plotOptions1]:radio").on("change", function(e){
				TurnOffButtons();				// the function is at the bottom of this file.
				
				if ( $("#radioButtonLas").prop("checked") ){
					$('#loadFile').css("visibility", "visible");
					$('#loadFile').css("display", "inline");
				}
				if ( $("#radioButtonExcel").prop("checked") ){
					$('#appendStep0').css("visibility", "visible");
					$('#appendStep0').css("display", "inline");
				}
			});
			
			$("#appendStep0").click(function() {
				// initialize the interface
				$("#ExcelUpdate1").css("visibility", "hidden");
				$("#divDepthFld").css("display", "none");
				$("#excelFieldList").css("display", "none");
				
				$("#btnResetZoom").css("visibility", "hidden");

				// populate the sheets list
				$("#sheet_name").find("option")
								.remove()
								.end();
									
				var options =  '<option value="">---- Select a sheet to plot ----</option>';
				for (var sheetname in EXCEL_CONTENT) {
					options += ('<option value="' + sheetname + '">' + sheetname + '</option>');
				}
				$("#sheet_name").append(options);
				
				$("#fldNameDepth").find("option")
								.remove()
								.end();
				$("#fldNameDepth").append('<option value="">---- Select the depth field ----</option>');
				
				$("#sheet_fields").empty();
				
				$(loadingWindow).jqxWindow('close');
				$("#modalExcel").modal('show');
			});
			
			$("#sheet_name").on("change", function(){
				var t = $(this).val();
				
				if (t != "") {
					$('#loadFile').css("visibility", "visible");
					//$("#loadFile").css("display","block");
				}
				else {
					$('#loadFile').css("visibility", "hidden");
					//$("#loadFile").css("display","none");
				}

				// populate the fields list
				$("#sheet_fields").empty();
				var opt = '<option value="">---- Select the depth field ----</option>';

				$("#fldNameDepth").find("option")
								.remove()
								.end();
										
				if (this.value != ""){
					$("#divDepthFld").css("display", "block");
					
					var sheet = EXCEL_CONTENT[this.value];
					var col_size = sheet.col_size;
					EXCEL_FIELDS = copyArray_excel_array(sheet.data[0]);
					
					
					EXCEL_TEMP_ARRAY.length = 0;
					EXCEL_TEMP_ARRAY = convertExcelToArray(sheet);				// in las_excel.js
					
					// to populate EXCEL_logLine_Name after remove the non-numeric fields
					IdentifyNumericFields();									// in las_excel.js
					
					for (var i=0; i<EXCEL_FIELDS.length; i++){
						if (EXCEL_NUMERIC_FLDS[i])
							opt += ('<option value="' + EXCEL_FIELDS[i] + '">' + EXCEL_FIELDS[i] + '</option>');
					}
				} else {
					$("#ExcelUpdate1").css("visibility", "hidden");
					$("#divDepthFld").css("display", "none");
					$("#excelFieldList").css("display", "none");				}
				
				$("#fldNameDepth").append(opt);
			});
			
			$("#fldNameDepth").on("change", function(){
				$("#sheet_fields").empty();
				
				if (this.value != ""){
					$("#ExcelUpdate1").css("visibility", "visible");
					$("#excelFieldList").css("display", "block");
					
					//var sf = '<table class="table table-striped table-condensed" style="width:400px; text-align: center;"><thead><tr><th style="width:75px; text-align: center;">Check to Select</th><th>Field Name</th></tr><thead><tbody>';
					var sf = '<table class="table table-striped table-condensed" style="width:400px; text-align: center;"><thead><tr><th>Field Name</th></tr><thead><tbody>';
					
					for (var i=0; i<EXCEL_FIELDS.length; i++){
						if (EXCEL_NUMERIC_FLDS[i] && EXCEL_FIELDS[i] != this.value){
							//sf += ('<tr><td style="text-align: center; padding:1px;"><input type="checkbox" name="fieldsName" value="' + i + '" style="padding-right:5px;"></td><td style="text-align: left; padding:1px;">' + EXCEL_logLine_Name[i] + '</td></tr>');
							sf += ('<tr><td style="text-align: left; padding:3px;">' + EXCEL_FIELDS[i] + '</td></tr>');
						}
					}
					sf += ('</tbody></table>');
				} else {
					$("#ExcelUpdate1").css("visibility", "hidden");
					$("#excelFieldList").css("display", "none");					
				}
				$("#sheet_fields").html(sf);
			});
			
			// re-organize excel data after all parameters about EXCEL are specified
			$("#ExcelUpdate1").click(function() {
				EXCEL_DEPTH_FLD = $("#fldNameDepth").val();						// defined in las_resources.js, and used in las_excel.js
				
				/*
				// This segment is for users to specify the plotting fields 
				EXCEL_PLOT_INDEX = new Array();
				$('input[name=fieldsName]:checked').each(function(i){
					EXCEL_PLOT_INDEX.push(parseInt($(this).val()));
				});
				*/
				
				ResetExcelLogLine(this.value);								// in las_excel.js
					
				// Convert Excel Reading to array
				ProcessExcelArray(EXCEL_TEMP_ARRAY);
				
				$('#modalExcel').modal('hide');
				Print_Unploted_Log_List();
			}); 
			
			/*
			 * Loaded the validated file
			 */
			var loadFile = document.getElementById('loadFile');
			loadFile.addEventListener('click', function(e) {
				if ( FILE_TYPE == "LAS") {
					initialDataHolder();
					
					BEGINNING_TIME = (new Date()).getTime();
					NO_CURVE_TO_PLOT = true;
					var file = fileInput.files[0];
				
					//var file = '<?php read_file(site_url('assets/file/O-CMS-001_KGAS-KINT-KOIL-KWTR-PIGN-VCL-SUWI.las')) ?>'
					// if the viewer open from third party with remote las file, this statement removes the remote file and reset the url to default.
					//window.history.pushState('Home', 'Gamabox-EP', base_apps_url+'/log');
					
					$("#btnResetZoom").css("visibility", "hidden");				// hide RESET ZOOM button
					//$("#btnSearchLasfile").css("visibility", "hidden");			// hide search las file button once the plots are loading
					//$("#canelFile").css("visibility", "hidden");				// hide cancel button once the plots are loading
					
					$("#fileInput").prop("disabled",true);
					$("#canelFile").prop("disabled",true);
					
					$(icProgress).css("visibility", "visible");
	
					var reader = new FileReader();
					reader.readAsText(file);
		
					reader.onload = function(e) {
						
						$("#txtProcess").html("Processing data...");
						console.log(currentTime() + ". ->Uploading Data");
						setTimeout(function(){
							processLasfile(reader.result);
							chartingLasdata();	
						}, 500);
					}
	
					reader.onloadend = function(e) {
						console.log(currentTime() + ". ->Data uploading successfully.");
						//$("#txtProcess").html("Plotting...");
						//setTimeout(function(){chartingLasdata();}, 500);
					}
	
					reader.onloadstart = function (e) {
						$(icProgress).css("visibility", "visible");
					}
				}
				
				if ( FILE_TYPE == "XLS" || FILE_TYPE == "XLSX") {
					FILENAME_LAS = FILENAME_EXCEL;
					
					$("#btnResetZoom").css("visibility", "hidden");
					
					initialDataHolder();
					// populate the sheets list
					
					$("#sheet_name").find("option")
									.remove()
									.end();
										
					var options =  '<option value="">---- Select a sheet to plot ----</option>';
					for (var sheetname in EXCEL_CONTENT) {
						options += ('<option value="' + sheetname + '">' + sheetname + '</option>');
					}
					$("#sheet_name").append(options);
					
					$("#fldNameDepth").find("option")
									.remove()
									.end();
					$("#fldNameDepth").append('<option value="">---- Select the depth field ----</option>');
					
					$("#sheet_fields").empty();
					
					$(loadingWindow).jqxWindow('close');
					$("#modalExcel").modal('show');
					
					//initialize the interface
					$("#ExcelUpdate1").css("visibility", "hidden");
					$("#divDepthFld").css("display", "none");
					$("#excelFieldList").css("display", "none");
				}
				
			});
			// end
			
			/*
			 * upload well top file
			 */
			var fileWellTop = document.getElementById('fileWellTop');
			fileWellTop.addEventListener('change', function(e) {
				if (fileWellTop.files.length>0){
					document.getElementById("uploadTopFile").value = fileWellTop.files[0].name;
					var file = fileWellTop.files[0];
					
					if ( file!=undefined ) {
						var filename = file.name;
						var fileType = filename.substring(filename.lastIndexOf(".") + 1, filename.length);
						if (fileType.toUpperCase() == "TXT") {
							$('#uploadTopFile').removeClass('file_error');
							$('#loadTopFile').css("visibility", "visible");
						}
						else
						{
							$('#uploadTopFile').addClass('file_error');
							
							$('#uploadTopFile').val("File not supported!");
							$('#loadTopFile').css("visibility", "hidden");
						}
					}
				}
				else
				{
					$('#uploadTopFile').val("File not supported!");
					$('#loadTopFile').css("visibility", "hidden");
				}
			});
			
			var loadTopFile = document.getElementById('loadTopFile');
			loadTopFile.addEventListener('click', function(e) {
				var file = fileWellTop.files[0];

				var reader = new FileReader();
				reader.readAsText(file);
	
				reader.onload = function(e) {
					setWellTop(reader.result);
				}

				reader.onloadend = function(e) {
					$('#loadingTopInfo').jqxWindow('close');
				}
			});
			
			$("#btnListSave").click(function(){
			    for (var i=1; i<logLine_Name.length; i++) {
			    	if ( $('#ckbx_' + i).is(':checked') ) {
			    		var n = Number($('#txtmin_'+ i).val().trim());
			    		var x = Number($('#txtmax_'+ i).val().trim());
			    		MIN_MAX_LOG_CURRENT[i] = [n, x];
			    	}
			    }
			        
				$('#modalLogList').modal('hide');
				$("#txtProcess1").html("Processing data...");
			    $('#loading_progress').modal('show');
			    
				BEGINNING_TIME = (new Date()).getTime();
				
				setTimeout(function(){
					chartingLasdata();
				}, 100);
			});			
			
			//end
		}
		
		function saveFileToServer(str){
			var request = $.ajax({
				url: "http://157.182.212.204/LasViewer/lasFileSave.php",
				data: {data: str, filename:FILENAME},
				type: "POST",
				dataType: "text"
			});

			request.success (function(){
				window.location.href = 'http://wayne/lasviewer/index.html?l=http://157.182.212.204/LasViewer/data/' + FILENAME;
				//window.open('http://wayne/lasviewer/index.html?l=http://157.182.212.204/LasViewer/data/' + FILENAME,'processLas');
			});
		}

		function onFileChange(ctrl){
			if (ctrl.files.length>0){
				FILENAME_LAS = fileInput.files[0].name;
				document.getElementById("uploadFile").value = FILENAME_LAS;
				var file = fileInput.files[0];
				
				if ( file!=undefined ) {
					var filename = file.name;
					var fileType = filename.substring(filename.lastIndexOf(".") + 1, filename.length);
					if (fileType.toUpperCase() == "LAS") {
						$('#uploadFile').removeClass('file_error');
						$('#loadFile').css("visibility", "visible");
						
						$("#excelSheets").css("display", "none");
					}
					else if (fileType.toUpperCase() == "XLSX"){
						$("#excelSheets").css("display", "inline");
						angular.element(this).scope().fileChanged(this.files);
					}
					else
					{
						$('#uploadFile').addClass('file_error');
						
						$('#uploadFile').val("File not supported!");
						$('#loadFile').css("visibility", "hidden");
					}
				}
			}
			else {
				$('#loadFile').css("visibility", "hidden");
				$('#uploadFile').val("Select a file");
			}
		}
		
        function DisplayIP(response){
        	FILENAME = FILENAME + "_"+ response.ip + ".las";	
        }
        
        function TurnOffButtons(){
			$('#loadFile').css("visibility", "hidden");
			$('#loadFile').css("display", "none");

			$('#loadFile').css("visibility", "hidden");
			$('#loadFile').css("display", "none");
			
			$('#appendStep0').css("visibility", "hidden");
			$('#appendStep0').css("display", "none");

        }

	});

});

