<?php
	class Home extends CI_Controller
	{
		function __construct(){
			parent::__construct();
			$this->load->library(array('cart','form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
		}	


		function index(){
			$session 	= $this->session->userdata('session');
			$role		= $this->session->userdata('role');
			
			if ($session == null && $role == null){
				$data['title']	 = "Login";
				$data['navbar']	 = " ";
				$data['content'] = $this->load->view('login', $data, TRUE);
				$this->load->view('frame-login',$data);
			}
			else{
				redirect('dashboard');
			}
			
		}


		function proses(){			
			$session_id = $this->uri->segment('3');
			$role = $this->uri->segment('4');
			$token = $this->uri->segment('5');
			$sesi=array(
                    'session'	=> $session_id,
                    'role'		=> $role,	
                    'token'		=> $token
                );
			$this->session->set_userdata($sesi);

			redirect('/dashboard');

		}

		

		function error(){
			$data['title']	= "404 Page Not Found";
			$this->load->view('frame-error',$data);
		}

		function tes(){
			$this->load->view('login_form');
		}

	}
?>