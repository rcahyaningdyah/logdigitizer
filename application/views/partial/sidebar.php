<?php 
			/*digunakan untuk menampung data session request cart yang sudah dipilih*/
			$request_cart = @$this->session->userdata('request_cart');
			/*var_dump($request_cart);*/
		?>
<div class="logopanel">
	<h1>
    	<a href="<?php echo site_url('') ?>"></a>
  	</h1>
</div>
<div class="sidebar-inner">
  	<div class="sidebar-top">
	    <form action="search-result.html" method="post" class="searchform" id="search-results">
	      <input type="text" class="form-control" name="keyword" placeholder="Pencarian">
	    </form>
	    <div class="userlogged clearfix">
	      	<i class="icon icons-faces-users-03"></i>
	     
	      	<div class="user-details">
	      		
		        <h4>Username</h4> 
		        <br/>
		       
	      	</div>
	    </div>
  	</div>
  	<div class="menu-title">
   		 Menu 
  	</div>
  	<ul class="nav nav-sidebar">
	    <li id="navhome"><a href=""><i class="icon-home"></i><span data-translate="Home">Home</span></a></li>
	    <li id="navmap"><a href=""><i class="icon-map"></i><span data-translate="Map">Map</span></a></li>
	    <li id="navtreeview"><a href=""><i class="icon-layers"></i><span data-translate="TreeView">TreeView</span></a></li>
	    <li id="navproduction"><a href=""><i class="icon-graph"></i><span data-translate="Production Report">Production Report</span></a></li>
	    <li id="navupload"><a href=""><i class="icon-arrow-up"></i><span data-translate="Batch Upload">Batch Upload</span></a></li>
	    <li id="navdocument"><a href=""><i class="icon-doc"></i><span data-translate="Documents">Documents</span></a></li>
	    <li id="navinfo"><a href=""><i class="icon-info"></i><span data-translate="Info & Help">Info & Help</span></a></li>
  	</ul>
  	<div class="sidebar-footer clearfix">
	    <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
	    	<i class="icon-size-fullscreen"></i>
		</a>
		<i class="icon-power"></i>
	   	 	
 	</div>
</div>
