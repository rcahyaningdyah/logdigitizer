<!-- css from template, used in all pages -->
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/theme.css'); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/ui.css'); ?>" >
<!-- end of css from template -->

<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/ajax/libs/prism/0.0.1/prism.min.css') ?>">

<!-- Optional theme -->
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/plugins/bootstrap/css/bootstrap-theme.min.css') ?>"> 
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/ajax/libs/prism/0.0.1/prism.min.css') ?>">


<link href="<?php echo site_url('assets/js/vendor/jquery_ui/jquery-ui.css') ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo site_url('assets/css/normalize.css') ?>">

<link rel="stylesheet" href="<?php echo site_url('assets/js/vendor/jqwidgets/widgets/styles/jqx.base.css') ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo site_url('assets/js/vendor/jqwidgets/widgets/styles/jqx.darkblue.css') ?>" type="text/css" />
<!-- Optional theme -->
<link rel="stylesheet" href="<?php echo site_url('assets/css/main.css') ?>"> 
<link rel="stylesheet" href="<?php echo site_url('assets/css/lasviewer.css') ?>" />

<script src="<?php echo site_url('assets/js/vendor/modernizr-2.7.1.min.js') ?>"></script>