<div class="header-left">
    <div class="topnav">
      <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
    </div>
</div>
<div class="header-right" >
	<ul class="header-menu nav navbar-nav">
		<!-- BEGIN USER DROPDOWN -->
		<li class="dropdown" id="user-header">
             <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                <span class="username"> Business Associate Name &nbsp;</span> 
                 <img src="<?php echo site_url('assets/images/avatars/avatar10.png') ?>" alt="user image">      
                    <span class="username"> &nbsp; Hi, Username</span>
            </a>
            <ul class="dropdown-menu">
            	<li>
                    <a href="<?php echo site_url('profile')  ?>"><i class="icon-user"></i><span>My Profile</span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-settings"></i><span>Account Settings</span></a>
                </li>
                <li>
                    <form  id="myform1" action="<?php echo $this->auth_lib->logout(); ?>" method="post" >
                        <input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token') ?>">
                        <a href="javascript: submitform()"><i class="icon-logout">
                            </i><span>Logout</span>
                        </a>
                    </form>
                </li>
            </ul>
		</li>	
		<!-- END USER DROPDOWN -->
		<!-- CHAT BAR ICON -->
        <li id= "notification-header">
            <a href="<?php echo site_url('/transaction/checkout') ?>"  >
                <i class="icon-basket" style="font-size: 20px"></i> 
                <span data-rel="tooltip" class="badge badge-primary badge-header" data-toggle="tooltip" data-placement="bottom" title="Checkout" >
                      <span id="update_cart">0</span>
                </span>
            </a>
            
        </li>

		<li id="quickview-toggle">
			<a href="#">
				<i class="icon-bell"></i>
				<span  class="badge badge-primary badge-header" id="notif-count">
                </span>
			</a>
		</li>

		<!-- END CHAT BAR ICON -->
	</ul>
</div>
