<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Profile</strong> View</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">Profile Well</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-area">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Transmittal</strong> request <?php echo $requestId; ?></h3>
      </div>
      <div class="panel-content content-profile to_pdf" id="fromHTMLtestdiv">
        <h2>Transmittal Letter</h2>
        <hr>
        <h5><strong>Detil Permohonan Dokumen</strong></h5>
        <ul id="detil-permohonan">
          <li>Pembuat Permohonan : </li>
          <li>Perusaaan Pembuat Permohonan : </li>
          <li>Tanggal Permohonan : </li>
          <li>Tujuan Permohonan : </li>
        </ul>
        <h5><strong>Detil Pengesahan Permohonan</strong></h5>
        <ul id="detil-pengesahan">
          <li>Tanggal Pengesahan : </li>
          <li>Pemberi Pengesahan : </li>
        </ul>
        <h5><strong>Daftar Dokumen</strong></h5>
        <table class="table">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Dokumen</th>
              <th>Dokumen Fisik</th>
              <th>Dokumen Digital</th>
            </tr>
          </thead>
          <tbody id="tabel-dokumen">
            <tr>
              <td>No</td>
              <td>Nama Dokumen</td>
              <td>Dokumen Fisik</td>
              <td>Dokumen Digital</td>
            </tr>
          </tbody>
        </table>
        <hr>
      </div>
      <button class="btn btn-primary" onclick="javascript:demoFromHTML(<?php echo $requestId; ?>)" class="button"><i class="icon-printer "></i> &nbsp;Print</button>
    </div>
  </div>
</div>
<script type="text/javascript">
  requestId = '<?php echo $requestId; ?>';
</script>

 