<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Profile</strong> View</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">Profile Well</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-document">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>Document</strong> Profile</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleDocument"><strong>Nama Document</strong> | Document ID</h2>
        <!-- README Tab Navigation -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-info"></i> Information</a></li>
          <li><a href="#tab2" data-toggle="tab"><i class="icon-eye"></i>Preview</a></li>
          <li><a href="#tab3" data-toggle="tab"><i class="icon-note"></i> History</a></li>
        </ul>
        <!-- README Tab Content -->
        <div class="tab-content tab-head">
          <!-- README TAB 1 -->
          <div class="tab-pane fade active in" id="tab1">
            
          </div>
          <!-- README TAB 2 -->
          <div class="tab-pane fade" id="tab2">
            
          </div>
          <!-- README TAB 3 -->
          <div class="tab-pane fade" id="tab3">
            <div class="row">
              <div class="col-md-2">
                <div id="tree-doctype-well" class="side-treeview"></div>  
              </div>
              <div class="col-md-10">
                <!-- TABLE DOC WELL -->
                <div class="table-responsive">
                  <div id="table-welldocument">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  documentId = '<?php echo $documentId; ?>';
</script>

 