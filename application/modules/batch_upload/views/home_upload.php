<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
	<h2><strong>Upload</strong> View</h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
	    <li><a href="<?php echo site_url('') ?>">Home</a>
	    </li>
	    <li class="active">File Upload</li>
	  </ol>
	</div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-main" id="panel-upload">
      <div class="panel-header panel-controls">
        <h3><i class="icon-layers"></i><strong>File</strong> Upload</h3>
      </div>
      <div class="panel-content content-profile">
        <h2 id="TitleUpload"><strong>Batch </strong> Upload</h2>
        <!-- README Tab Navigation -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#upload-tab1" data-toggle="tab"><i class="icon-info"></i> File/Files Upload</a></li>
          <li><a href="#upload-tab2" data-toggle="tab"><i class="icon-eye"></i>Upload Template</a></li>
        </ul>
        <!-- README Tab Content -->
        <div class="tab-content tab-head">
          <!-- README TAB 1 -->
          <div class="tab-pane fade active in" id="upload-tab1">
            <!-- NEW UPLOAD -->
            <div class="text-center">
              <p class="text-center">*please use upload template from gamabox-ep system (on the next tab)</p>
              <form action="<?php echo $this->auth_lib->url(); ?>/submit/batch/67" method="post" enctype="multipart/form-data" class="forms" id="form-batch-upload">
                <input type="hidden" name="_csrf" value="<?php echo $this->session->userdata('token'); ?>">
                <input type="file" name="file_upload" id="files-gamabox">
                <input type="hidden" name="value" value="" id="value-form">
                <input type="hidden" name="fileName" value="" id="fileName-form">
                <br>
                <button type="button" class="btn btn-primary m-r-20 text-center" id="check-parse-gamabox">Check File</button>
                <div id="afterparse-table-gamabox">
                </div>
                <button type="submit" class="btn btn-success m-r-20 text-center" id="submit-form">Upload</button>
              </form>  
            </div>
            
            <hr>
          </div>
          <!-- README TAB 2 -->
          <div class="tab-pane fade" id="upload-tab2">
            <div id="template_table">
              
            </div>
          </div>
        </div>
        <!-- END Tab  -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  
</script>

 