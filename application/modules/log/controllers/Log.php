<?php
	class Log extends CI_Controller
	{	
		function __construct(){
			parent::__construct();
			$this->load->library(array('cart','form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
		}	

		function index(){
			$data['title']			= "Log Digitizer";
			$data['content']		= $this->load->view('home_log', $data, TRUE);
			$data['content_script'] = $this->load->view('script', $data, TRUE);
			
			$this->load->view('frame-log',$data);
		}

		function tes_log(){
			$data['title']			= "Log Digitizer";
			$data['content']		= $this->load->view('tes_log', $data, TRUE);
			$data['content_script'] = $this->load->view('script', $data, TRUE);
			
			$this->load->view('frame-log',$data);
		}

	}
?>