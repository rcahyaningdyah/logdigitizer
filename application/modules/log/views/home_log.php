<div class="row">
    <div id="page-wrapper">
    	 <!-- Begin Toolbar -->
        <div id="lv_toolbar">
            <div style="float:left">
                <button id="iconOpen" type="button" class="btn btn-primary" style="padding: 2px 2px;" >
                    <span class="glyphicon glyphicon-upload lvButton"  data-toggle="popover" data-placement="bottom" data-content="Upload LAS file"></span>
                    <span data-rel="tooltip" data-toogle="tooltip" data-placement="bottom" title="Upload LAS file or Excel (.xlsx) file">Upload File</span>
                </button>

                <button id="iconOpenLogList" type="button" class="btn  btn-primary"  style="padding: 2px 2px;">
                    <span class="glyphicon glyphicon-barcode lvButton" data-toggle="popover" data-placement="bottom" data-content="Assign curves into different tracks"></span>
                    <span data-rel="tooltip" data-toogle="tooltip" data-placement="bottom" title="Assign curves into different tracks">Assign Tracks</span>
                </button>
            </div>
        </div>
        <!-- End Toolbar -->
    </div>

    <!-- data dictionary for log list in different tracks -->
    <div id="helpLogList" class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm ">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        ×
                    </button>
                    <span id="helpTitle">Modal Heading</span>
                </div>
                <div id="helpBody" class="modal-body">
                    content
                </div>
            </div>
        </div>
    </div>

    <!-- progress bar for reassign log lists to tracks -->
    <div id="loading_progress" class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-vertical-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <!--
                    <button class="close" data-dismiss="modal">
                    ×
                    </button>-->
                    <span><b>Status</b></span>
                </div>
                <div class="modal-body center-block">
                    <span id="txtProcess1"  class="center-block" style="color: #FF0000; text-align: center;">Data is being processed ......</span>
                </div>
            </div>
        </div>
    </div>

    <!-- The list of logs -->
    <div id="modalLogList" class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg"  style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #fcfcfc; text-align:center">
                    <button class="close" data-dismiss="modal">
                        ×
                    </button>
                    <span id="LogListTitle" align="center"  style="font-size: 16px; font-weight: bold">Logs in Uploaded File</span>
                </div>
                <div class="modal-body"  style="font-size: 12px;height: 610px; overflow-y:auto; clear:both">
                    <div id="unLogList" style="width: 750px; float: left;">

                    </div>
                </div>
                <div class="modal-footer" style="background-color: #fcfcfc;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                    <!--<button type="button" class="btn btn-primary" onclick="RePlotLogsList()" id="btnListSave">-->
                    <button type="button" class="btn btn-primary" id="btnListSave">
                        <!-- RePlotLogsList is defined in las_list.js -->
                        Save changes and plot
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalExcel" class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 600px;">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #fcfcfc; text-align:center">
                    <button class="close" data-dismiss="modal">
                        ×
                    </button>
                    <span id="modelExcelTitle" align="center"  style="font-size: 16px; font-weight: bold">Configure Excel Fields for Plotting</span>
                </div>
                <div class="modal-body"  style="font-size: 14px;height: 610px; overflow-y:auto; clear:both">
                    <div id="excelSheetsList" style="width: 550px; height: 40px; float: left;">
                        Step 1. Choose sheet:
                        <select id="sheet_name" class="form-control sheet_select" style="float:right;">
                            <option value="">---- Select a sheet to plot ----</option>
                        </select>
                    </div>
                    <div id="divDepthFld" style="width: 550px; float: left;">
                        <div>
                            Step 2. Specify the Depth field:
                            <select id="fldNameDepth" class="form-control sheet_select" style="float:right;">
                                <option value="">---- Select the depth field ----</option>
                            </select>
                        </div>
                        <div id="unitInfo" style="clear: both; font-style:italic; color:#A6A53B; padding-left: 48px;">(Note: Depth must be in FEET.)</div>
                    </div>
                    
                    <div id="excelFieldList" style="width: 550px; float: left;">
                        Step 3. Fields that can be ploted. You can specify how to plot each field in next step.
                        <div id="sheet_fields" style="padding-left: 50px;">
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: #fcfcfc;">
                    <!--
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                    -->
                    <button type="button" class="btn btn-primary" id="ExcelUpdate1">
                        <!-- RePlotLogsList is defined in las_list.js -->
                        Next
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Load las file window -->
    <div id="loadingWindow">
        <!-- <div style="background-color:#319DB5">
            <div class="windowHeader">
                Well Log Viewer
            </div>
        </div><br> -->
        <div class="divSelectFile">
            <div class="windowHeader">
                <h3><strong>Well Log Viewer</strong></h3>
            </div>
            <div class="intro">
                Well Profile from Log ASCII Standard (<b>LAS</b>) files
                <br>
            </div>
            <div class="intro" style="height:140px">
                <div style="width:150px; float: left; padding-top: 5px">
                    <img src="<?php echo site_url('assets/images/Las_icon.png') ?>">
                </div>
                <div style="float: left; height: 130px">
                    The viewer automatically displays:
                    <br>
                    1. GR/SP/CAL/Temp Logs
                    <br>
                    2. Resistivity-Induction Logs
                    <br>
                    3. Porosity-Density-PE Logs
                    <br>
                    4. Spectral GR/Sonic Logs
                    <br>
                    5. Computed Logs
                    <br>
                    6. Formation  Logs
                    <br>
                </div>
            </div>
            <div ng-app="App" style="height:80px;" >
                <!--<div ng-controller="ngCtrl_viewerdata" class="left_wrap">-->
                <div ng-controller="ngCtrl_viewerdata">
                    <form  action="" method="post" enctype="multipart/form-data" style="padding-left:80px">
                    <!--
                    <div style="font-size: 12px; display: inline;">
                        <input type="radio" id="radioButtoLas" name="fileTypes" style="display: inline;" checked="true">Upload LAS file
                        <input type="radio" id="radioButtoExcel" name="fileTypes" style="display: inline;">Upload Excel file
                    </div>
                    -->
                    <input id="uploadFile" placeholder="Choose LAS File" disabled="disabled" class="form-control" style="background-color:#F1F2F3;display:inline; height:30px; width:250px; font-size: 14px;"/>
                    <div id="btnSearchLasfile" class="fileUpload btn btn-primary">
                        <span>Search</span>
                        <input id="fileInput" type="file" class="upload" accept=".las,.xlsx">
                    </div>
                    
                    <button type="button" id="loadFile" class="btn btn-primary" style="display: none;">
                        Plot
                    </button>
                    <button type="button" id="appendStep0" class="btn btn-primary" style="display: none;">
                        Next
                    </button>
                   <!--  <button type="button" id="canelFile" class="btn btn-primary" style="visibility: hidden; float: right">
                        Cancel
                    </button> -->
                    
                    <!-- plot type: new or append -->
                    <div id="divExcelProcessing" style="padding-top:5px; font-size: 12px; color:red; display: none;">
                        Prepocessing EXCEL file...    
                    </div>
                    
                    <div id="plotOptions" style="font-size: 13px; display: none;">
                        Plot options:
                        <input type="radio" id="radioButtonLas" name="plotOptions1" style="display: inline;">Plot as new graph
                        <input type="radio" id="radioButtonExcel" name="plotOptions1" style="display: inline;">Append to current graph
                    </div>
                    <!-- end: plot type: new or append -->
                    <!--
                    <div id="excelSheets" style="display: none; font-size: 12px">
                        Sheet Name: 
                        <select id="sheet_name" class="form-control sheet_select" ng-change="updateJSONString()" ng-model="selectedSheetName" required="true" ng-required="true" ng-options="sheetName as sheetName for (sheetName, sheetData) in sheets">
                            <option value="">---- Select a sheet ----</option>
                        </select>
                        <div style="font-size: 12px; display: inline; margin-left: 70px;">
                            <input type="checkbox" id="excel_firstRow" style="display: inline; margin-right: 5px;" checked="true">First row include field names
                        </div>
                        
                    </div>
                    -->
                    </form>
                </div>
                <!--
                <div  class="right_wrap">
                    <div class="right1" style="height: 20px; font-size: 12px;">
                        
                    </div>
                    <div class="right2" style="float:right">
                        <button type="button" id="canelFile" class="btn btn-primary" style="visibility: visible; float: right">
                            Cancel
                        </button>
                    </div>
                </div>
                -->
            </div>
            
            <div id="icProgress" class="center-block">
                <span id="txtProcess" class="center-block" style="color: #FF0000; text-align: center;">Data is being processed ......</span>
            </div>
        </div>
    </div>


    <!-- Load Well Top information Window -->
    <div id="loadingTopInfo">
        <div style="background-color: #bfccd9;">
            <div class="windowHeader">
                Load Well Log Top Information
            </div>
        </div>
        <div class="divSelectFile" style="overflow-y: hidden">
            <div style="font-size:13px; font-style: italic; background-color: #fcfcfc;">
                <b>The formation top text (.txt) file should be in the following format</b>
                (formation name and depth are seperated by colon (:)).
            </div>
            <div id="wt_sample" class="intro" style="margin:auto; height: 130px"></div>
            <div style="float: right; height: 20px; padding-top: 10px; font-size: 12px;">
                <a href="data/4704105432_Tops.txt" target="_blank" title="Right click to save file to you local machine" alt="Right click to save file to you local machine">A sample well top file</a>
            </div>
            <div style="padding: 5px 10px 5px 20px;">
                Upload formation top file
                <br>
                <input id="uploadTopFile" placeholder="Choose File" disabled="disabled" style="height:30px; width: 150px;" />
                <div class="fileUpload btn  btn-primary">
                    <span>Search</span>
                    <input id="fileWellTop" type="file" class="upload" accept="text/*"/>
                </div>
                <button type="button" id="loadTopFile" class="btn btn-primary" style="visibility: hidden">
                    Upload
                </button>
            </div>
        </div>
    </div>


    <!-- body lasviewer graphic -->
    <div id="viewerBody" style="width:500px;">
        <!-- well information after proessing -->
        <div id="divCtrl">
            <div id="cursor_info" class="well_info"></div>

            <div id="las_Well_info" class="well_info">
                <div class="barHeader" style="display: block;">
                    <span id="wellInfoHeader" class="glyphicon glyphicon-chevron-down SectionHeaderSign "></span>Well Information
                </div>

                <div id="las_Well_info1"></div>
            </div>
            <!-- populated in las_header.js-->

            <div id="las_Well_top" class="well_info">
                <div class="barHeader" style="display: block;">
                    <span id="wellTopHeader" class="glyphicon glyphicon-chevron-down SectionHeaderSign "></span>Formation Tops
                </div>
                <div id="las_Well_top1"></div>
                <div id="noTops" style="display: none; height:60px">
                    <span class="segError">No formation tops. Click to upload formation top text (.txt) file.</span>
                    <div id="iconNoTopOpen" class="iOpenFolder" style="margin-left: 230px; font-size:14px">
                        <span class="glyphicon glyphicon-upload" data-toggle="popover" data-placement="bottom" data-content="Upload formation top file" style="font-size: 18px"></span>
                    </div>
                </div>
            </div>
            <!-- populated in las_header.js-->

            <div id="las_Well_Curve_Info" class="well_info">
                <div class="barHeader" style="display: block;">
                    <span id="curveInfoHeader" class="glyphicon glyphicon-chevron-right SectionHeaderSign "></span>Curve Information
                </div>
                <div id="las_Well_Curve_Info1"></div>
            </div>
            <!-- populated in las_header.js-->

            <div id="las_track_list" class="well_info">
                <div class="barHeader">
                    <span id="trackOrders" class="glyphicon glyphicon-chevron-down SectionHeaderSign "></span>Track Orders
                </div>
                <div id="las_track_list1"></div>
            </div>
            <!-- populated in las_charting.js-->
        </div>
        <!-- end well innformation after processing -->

        <!-- begin chart after processing -->
        <div id="lasViewer_wrapper">
            <div id="lasPlotHeader" class="plotHeader">
                <div id="chart_0_depth_0" class="plotHeader chart_0_depth"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_1_lgr_0" class="plotHeader chart_1_lgr"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_2_ref_0" class="plotHeader chart_2_ref"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_3_dual_0" class="plotHeader chart_3_dual"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_4_litho_0" class="plotHeader chart_4_litho"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_7_sgr_0" class="plotHeader chart_7_sgr"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_8_sgr_ratio_0" class="plotHeader chart_8_sgr_ratio"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_9_custom1_0" class="plotHeader chart_9_custom"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_16_ctuk_0" class="plotHeader chart_16_ctuk"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_18_formation_0" class="plotHeader chart_18_formation"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->
                <button id="btnResetZoom" type="button" class="btn btn-primary btn-sm">
                    Reset Zoom
                </button>
            </div>
            <div id="lasPlotBody" class="plotBody">
                <div id="chart_0_depth" class="chart_0_depth"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_1_lgr" class="chart_1_lgr"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->
                
                <div id="chart_2_ref" class="chart_2_ref"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_3_dual" class="chart_3_dual"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_4_litho" class="chart_4_litho"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_7_sgr" class="chart_7_sgr"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->
                
                <div id="chart_8_sgr_ratio" class="chart_8_sgr_ratio"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_9_custom1" class="chart_9_custom"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_16_ctuk" class="chart_16_ctuk"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->

                <div id="chart_18_formation" class="chart_18_formation"></div>
                <!-- triggered in las_chating.js and populated in las_charting_log.js-->
                <div id="fileDisplayArea" style="width:0px;"></div>
            </div>
        </div>
        <!-- end chart after processing -->
    </div>
</div>

<script type="text/javascript">
    document.getElementById("iconOpen").onclick = function () {
        location.href = "";
    };
    
</script>