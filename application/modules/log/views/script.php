<script type="text/javascript" src="<?php echo site_url('assets/plugins/angularjs/1.0.7/angular.min.js') ?>"></script>
<!-- <script type="text/javascript" src="js/vendor/jquery-1.10.2.min.js"></script> -->
<script type="text/javascript" src="<?php echo site_url('assets/plugins/jquery/jquery-1.11.1.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/ajax/libs/lodash.js/2.4.1/lodash.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js') ?>"></script>
<!-- <script type="text/javascript" src="js/vendor/jquery_ui/jquery-ui.js"></script> -->

<script type="text/javascript" src="<?php echo site_url('assets/js/plugins.js') ?>"></script> <!-- Main Plugin Initialization Script -->
<script type="text/javascript" src="<?php echo site_url('assets/plugins/bootstrap/js/tooltip.js')?>"></script>

<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/Highcharts/js/highcharts.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/Highcharts/js/highcharts-more.js')?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/Highcharts/js/modules/exporting.js')?>"></script>

<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/jqwidgets/widgets/jqxcore.js')?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/jqwidgets/widgets/jqxbuttons.js')?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/jqwidgets/widgets/jqxwindow.js')?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/jqwidgets/widgets/jqxscrollbar.js')?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/vendor/jqwidgets/widgets/jqxpanel.js')?>"></script>

<script type="text/javascript" src="<?php echo site_url('assets/js/xls/jszip.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/xls/xlsx.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/xls/xls.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/xls/xlsx-reader.js') ?>"></script>

<script src="<?php echo site_url('assets/js/las/las_resources.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_help.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/utils.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/data_error.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_list.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_header.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_data.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_chart.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_charting_logs_header.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_charting_logs.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_excel.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/las/las_main.js'); ?>"></script> 

<script type="text/javascript">
	function DisplayIP(response) {
		FILENAME = FILENAME + "_" + response.ip + ".las";
	}
</script> 